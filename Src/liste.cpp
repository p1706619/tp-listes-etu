#include "liste.hpp"

#include <iostream>
#include <cassert>

using namespace std;

Liste::Liste() {

  prem = NULL;
  last = NULL;
  
}

Liste::Liste(const Liste& autre) {

  prem = autre.prem;
  last = autre.last;
}

Liste::~Liste() {
   while (!estVide()) supprimer_en_tete();

}

bool Liste::estVide () const {
    return prem == NULL;
}
Liste& Liste::operator=(const Liste& autre) {
  this->~Liste();
    Cellule * temp = autre.prem;
    while (temp != NULL) {
        ajouter_en_queue(temp->valeur);
        temp = temp->suivante;
    }
    return *this ;
}

void Liste::ajouter_en_tete(int valeur) {

  Cellule * temp = new Cellule;
  temp->valeur = valeur;
  temp->suivante = prem;

  if (estVide()) last = temp;
    
  prem = temp;
}

void Liste::ajouter_en_queue(int valeur) {

    if (prem == nullptr) ajouter_en_tete(valeur);
  else {
      Cellule * temp = new Cellule;
      temp->valeur = valeur;
      temp->suivante = NULL;

      last->suivante = temp;
      last = temp;
    }
}

void Liste::supprimer_en_tete() {

    Cellule * temp = prem;
    prem = prem->suivante;

    delete temp;

}

Cellule* Liste::tete() {

    return this->prem;
}

const Cellule* Liste::tete() const {

  return  this->prem;
}

Cellule* Liste::queue() {

    return this->last;
}

const Cellule* Liste::queue() const {

  return this->last;
}

int Liste::taille() const {

  int compteur = 0;
  Cellule * temp = prem;

  while (temp != nullptr)
  {
      compteur ++;
      temp = temp->suivante;
  }

  return compteur ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule * temp = prem;
  bool trouve = false;

  while ((temp != NULL) && !trouve) {
      if ( temp->valeur == valeur) trouve = true;
      else {
          temp = temp->suivante;
      }
  }
  if (trouve) return temp;
  else return nullptr;

}

const Cellule* Liste::recherche(int valeur) const {
  Cellule * temp = prem;
  bool trouve = false;

  while ((temp != NULL) && !trouve) {
      if ( temp->valeur == valeur) trouve = true;
      else {
          temp = temp->suivante;
      }
  }
  if (trouve) return temp;
  else return nullptr;
}

void Liste::afficher() const {

Cellule * temp = prem;
    while (temp != NULL) {
        cout << temp->valeur << " ";
        temp = temp->suivante;
    }
    cout << endl;
    
}

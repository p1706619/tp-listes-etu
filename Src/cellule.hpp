#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

#include <vector>

class Cellule {

  public:
  int valeur;
  std::vector <Cellule *> suivants;
 
  Cellule();
  Cellule(int);
  ~Cellule();
  

  int getNiveau();
  
  


} ;

#endif

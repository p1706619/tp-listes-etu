  
#ifndef __SKIP_LIST_H__SH_
#define __SKIP_LIST_H__SH_

#include <vector>
#include <stdlib.h>
#include <stack>

#include "cellule.hpp"

class SkipList{
	
	private:
	Cellule * bidon;
	
	
	
	public:
	SkipList();
	~SkipList();
	
	void insertion(int);
	Cellule* recherche(int);
    

};

#endif
